import moment from 'moment';
import _ from 'lodash';
import './style.css';

const formatTime = (left) => `${_.padStart(left.minutes(), 2, '0')}:${_.padStart(left.seconds(), 2, '0')}`;

function get(name) {
  if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
     return decodeURIComponent(name[1]);
}

function startCountdown(clock, form, totalMillis = 60000) {
  const progress = clock.querySelector('.progressvalue');
  const timeDisplay = clock.querySelector('.timedisplay');
  if (clock.timer) {
    clearInterval(clock.timer);
    timeDisplay.style.color = timeDisplay.style.borderColor = '';
    progress.style.width = '';
  }
  form.classList.add('running')
  const end = moment().add(totalMillis, 'milliseconds');
  
  clock.timer = setInterval(() => {
    const left = moment.duration(end.diff(moment()));
    timeDisplay.innerText = formatTime(left);
    const percentage = 100 * (1 - left.asMilliseconds() / totalMillis);
    progress.style.width = `${percentage}%`;
    if (left.asSeconds() <= 0) {
      timeDisplay.style.color = timeDisplay.style.borderColor = 'darkred';
      clearInterval(clock.timer);
      form.classList.remove('running');
    }
  }, 100);
}

window.onload = () => {
  const clock = document.getElementById('clock');
  const form = document.getElementById('setform');
  form.onsubmit = (ev) => {
    ev.preventDefault();
    const selectedMinutes = parseInt(form.querySelector('input[name="minutes"]').value);
    startCountdown(clock, form, selectedMinutes * 60000);
  }
  const getTime = get('time');
  if (getTime) {
    const selectedMinutes = parseInt(getTime);
    startCountdown(clock, form, selectedMinutes * 60000);
  }
}